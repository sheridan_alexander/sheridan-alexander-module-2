class App{
	String name="FNB Banking App";
	String sector="Best Overall App";
	String developer="Nomalanga Nkosi";
	String yearWon="2012";

	String getAppName(){
		return name;
	}

	String getSector(){
		return sector;
	}

	String getDeveloper(){
		return developer;
	}

	String getYearWon(){
		return yearWon;
	}

	String capitalName(String aName){
		return aName.toUpperCase();
	}

	void printInfo(String name,String sector,String developer,String awardYear){
		print("Name of the app: "+name);
		print("Sector: "+sector);
		print("Developer of the app: "+developer);
		print("The year this app won the app of the year award is: "+awardYear);
	}
		
}

void main(){
	App nApp=new App();
	String name=nApp.getAppName();
	String sector=nApp.getSector();
	String dev=nApp.getDeveloper();
	String year=nApp.getYearWon();

	nApp.printInfo(name,sector,dev,year);
	String capName=nApp.capitalName(name);
	print("");
	print(capName);
}
